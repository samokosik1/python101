#!/bin/python3

#Print a string
print("Printing...")
print("Printing2...")
print( """
This 
is 
a
multi-line
string."""
)
print("Hello"+"bye")
print("\n") #new line

#Math
print("Math time: ")
print(50+50) #add
print(50-50) #subtract
print(50*50) #multiply
print(50/50) #divide
print(50**50) #exponents
print(50%6) #modulo
print(50//6) #number without leftovers
print("\n")

#Variables and Methods
print("Variables and methods.")
quote = "All is fair in love and war."
print(len(quote)) #length
print(quote.upper())
print(quote.lower())
print(quote.title())
name = "Samo" #str
age = 15 #int
average_grade: 6.8 #float
print(int(age))
print(int(29.9)) #prints 29 - does not round
print("My name is " + name + " " + "and I am " + str(age) + " " + "years old")
print("\n")
age += 1
print(age)
print("\n")

#Functions
print("Now, some functions: ")
def who_am_i():
    name1 = "Samko"
    age = "15"
    print("My name is " + name1 + " " + "and I am " + str(age) + " " + "years old")
who_am_i()

#adding new parametres 
def add100(num):
    print(num+100)
add100(200)

#adding multiple parametres
def add(x,y):
    print(x+y)
add(3,10)

#using return
def multiply(x,y):
    return x*y
print(multiply(12,44))
print("\n") 

#Boolean expressions (True or False)
print("Boolean expressions")
bool1 = True
bool2 = 3*3 == 9
bool3 = False
bool4 = 3*3 == 17
print(bool1, bool2, bool3, bool4)
print(type(bool4))

#Realtional and Boolean operators
greater_than  = 7>5
less_than = 5<7
greater_than_equal_to = 7>=7
lesser_than_equal_to = 7<=7
print(greater_than, less_than, greater_than_equal_to, lesser_than_equal_to)
test_and = (7>5) and (5<7)
test_or = (7>5) and (5<7)
test_not = not True
print("\n") 

#Conditional statements
print("Conditional statements:")
def soda(money):
    if money >= 2:
        return "You have got a soda."
    else:
        return "No soda ):"
print(soda(2))
print(soda(3))

def alcohol(age, money):
    if age >= 18 and money >= 5:
        return "We are getting tipsy"
    elif age >= 18 and money < 5:
        return "Come back with more money"
    elif age < 18 and money >= 5:
        return "Nice try, kid"
    elif age < 18 and money < 5:
        return "You are too young and too poor"
print(alcohol(21,5))
print("\n") 

#Lists
print("Lists have brackets")
movies = ["When Harry met Sally", "The Hangover", "The perks of being a wallflower", "The Exorcist" ]
print(movies[1])
print(movies[0:2])
print(movies[1:]) #slicing
print(movies[:1])
print(movies[-1]) #last item of the list
print(len(movies))
movies.append("JAWS")
movies.pop() #pop removes the last item in the list
movies.pop(1) #now the 2nd

movies = ["When Harry met Sally", "The Hangover", "The perks of being a wallflower", "The Exorcist" ]
person = ["Heath", "Jake", "Leah", "Jeff"]
combined = zip(movies, person)
print(list(combined))

#Tuples
print("Tuples have parentheses and cannot change")
grades = ("A", "B", "C", "D", "E", "F")
print(grades[1])
print("\n")

#Looping
print("For loops - start to finish iterate:")
vegetables = ["cucumber", "spinach", "cabbage"]
for x in vegetables:
    print(x)

print("While loops - executes as long ad True?")
i = 1
while i < 10:
    print(i)
    i = i+1



# Create an http server
python -m http.server

# Create an ftp server
(install pyftpdblib first)
python -m pyftpdlib -p 21 -w
